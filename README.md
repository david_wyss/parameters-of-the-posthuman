# MDEF - Challenge 3 (27/04/2021 - 30/04/2020) - (Team 2) 
### Roger Guilemany - [personal webpage](https://roger_guilemany.gitlab.io/mdef-website/term2.html) <br> David Wyss - [personal webpage](https://david_wyss.gitlab.io/mdef-class/36_FabChallenge3.html) <br> Krzysztof Wronski - [personal webpage](https://krzysztof_wronski.gitlab.io/wrona/projects.html)

##### Rubric
1. Initial idea/Concept of the Project                                          **(done)**
2. Purpose (What is supposed to do or not to do)                                **(done)**
3. Shown how your team planned and executed the project                         **(done)**
4. System diagram (illustration explaining function, parts, and relations)      **(done)**
5. Integrated & Honest Design (How you designed it & use of tech)               **(done)**
6. How did you fabricate it (fabrication processes and materials)               **(done)**
7. BOM & Iteration Process (Build of Materials)                                 **(done)**
8. The coding Logic (Algorithms and flowcharts, pseudocoding)                   **(done)**
9. Iteration Process - Described problems and how the team solved them          **(done)**
10. Listed future development opportunities for this project                    **(done)**
11. Photographies of the end artifacts                                          **(done)**
12. Design & Fabrication files (Open Source)                                    **(done)**
13. Link to your individual pages                                               **(done)**

---
## Design Practice Self-Assessment

This is the documentation for our third challenge from Fabacademy, in collaboration with IAAC and the MDEF programme.
<br><br>

### 1. Concept
#### Goal
Design and make “a tool or strategy” that conviend with your artifacts help you to measure the impact of your interventions.
<br>

#### The Project Idea
Our project is called 'Design Practice Self-Assessment'. By building upon the idea of quantifying the impact of our design practice over time, our group wanted to develop a speculative assessment tool that questions and characterises the scope and dimension of posthumanist practices.
<br><br>
<img src="Images/Introduction/1.jpg" alt="drawing" width="500"/>
<br>

The project has been developed in the form of a physical self-assessment tool that allows users to assess the performance of a design project - or various projects due to an average function - by taking into account posthumanist criterias. The tool is conceived as a dynamic interaction and experimentation space that collects and displays data with the aim of encouraging reactions and sharing opinions. 
<br>

#### Purpose
'Design Practice Self-Assessment' is a collective exploration that aims to reflect on and think about alternative matrics used to define success and performance beyond today's traditional matrics. Conceived to transcend the limits of time and conventional formats, this project explores new ways on how designers could be encouraged to think about posthumanism as well as their impact on industry, people, social systems and cultural values. As research in this area is still mainly uncovered, various possibilities as well as a wide set of criterias have emerged throughout our exploration process. 
<br><br>
<img src="Images/Final/1.jpg" alt="drawing" width="400"/>
<img src="Images/Final/2.jpg" alt="drawing" width="400"/>
<br>

#### Functionalities 
The 'Design Practice Self-Assessment' allows the user(s) to:
- Select five key posthumanist matrics 
- Assess current project(s) along choosen matrics on a scale from 1-10
- Take the average across all past projects
- Create awareness around projects' overall posthuman performance
<br><br><br><br> 


### 2. Planning & Development 
The process of creating the physical artifact was divided into four main parts with each one person in charge: _3D Design & Fabrication (Krzysztof)_, _Electronics(Roger)_, as well as _Molding & Casting (David)_. Throughout the process, we planned and worked on four key design elements:

- 3D-Designed Concept, built upon pre-used HDPE <br> 
- 3D-Printed Mold Elements, rendered on Rhino<br> 
- 2 Types of Buttons, created through molding & casting <br> 
- Code Logic & Electronics, wired on a back panel

#### Iteration Process
The first step was to create a 3D prototype, where we reflected on the parts to be included in the artifact. After that we started to build a 3D model in Rhino, including both the design of the general artifact as well considering the smaller parts of it (e.g. buttons). In a third step, we then used the CNC to cut the two panels for the artifact and applied the methodology of molding & casting to develop all necessary buttons. In parallel to the design, we took a lot of efforts in working on and developing the code logic and finalise the electronics that were required. In a last step, we wired the cables and brought all parts together.
<br> 

**First Iteration:** 3D modelling of the prototype in real dimensions.<br> 
**Second Iteration:** 3D-Design of parts and pieces (front panel and mold for buttons).<br> 
**Third Iteration:** CNC Cut of panel & cast of buttons. <br> 
**Fourth Iteration:** Develop the code logic and electronics. <br> 
**Fifth Iteration:** Wire the electronics and install all cables to the back panel.<br> 
**Sixth Iteration:** Bring all parts together & finalise artifact.<br> 

Below, you can find a more in-depth documentation of the 3D Design & Fabrication, Molding & Casting & Electronics: 
<br><br><br>

#### Step by Step Guide: 3D Design & Fabrication 
We decided our physical enclosure would be assembled of two flat materials sandwiching electronics and an array of LEDs for use as a display in between. Our back-panel, a found piece of fibre-board would not require any digital fabrication and was simply cut to size. The back-panel would hold our bread-board, micro-controller, and wiring in place. Our front panel was designed digitally to create openings for our desired buttons and light arrays. The front-panel would hold LED strips, buttons, and silicone-button pads in place.

*1. 3D-Design of the front panel:*<br>
Any flat piece of material can be used for the front-panel, but this is a chance to define the dominant aesthetic and tell a story about the material chosen for the project. While we considered the use of acrylic for our front-panel, we wanted to avoid the use of virgin material for our project. We decided to use the precious plastics set-up in the Barcelona Fablab to create our own flat piece of material using HDPE from used bottle-caps and bottles.
<br><br>
<img src="Images/Panel/0.jpg" alt="drawing" width="400"/>
<br><br>

*2. Use precious plastics to fabricate the panel:*<br>
While the precious plastics set-up includes a grinder to grind up the cut-up pieces of used HDPE, we used some pre-ground HDPE already stored in the fab-lab. The first step, is to pour the ground up plastic evenly on a metal plate to compress it with heat. The plastic material is sandwiched between two aluminium plates, with additional pieces acting as a frame to seal the fibres around the desired dimensions. This is then inserted into a hydraulic press with heating elements top and bottom. The hydraulic press is raised to 5 bar and the temperature to circa 210 C. After 20 minutes, the plastic fibres fused and compressed into, in our case, a 41x41cm panel of 5,2mm thickness.
<br><br>
<img src="Images/Panel/1.jpg" alt="drawing" width="290"/>
<img src="Images/Panel/2.jpg" alt="drawing" width="290"/>
<img src="Images/Panel/2.1.jpg" alt="drawing" width="290"/>
<img src="Images/Panel/2.gif" alt="drawing" width="123"/>
<br><br>

*3. Use CNC to cut the panel:*<br>
Because we used HDPE for our front-panel material, the laser cutter could not be used. We determined the dimensions of our front-panel based on the usable width of our material and used Adobe Illustrator and Rhino to design the 2-D files which would then be used for Rhino-CAM on the CNC. The 2-D design was made in coordination with the button mold design to ensure the dimensions would match and in coordination with the electronics design to make sure the holes for the emitting light would be perfectly aligned with the LED strips housed underneath.
<br><br>
<img src="Images/Panel/3.jpg" alt="drawing" width="290"/>
<img src="Images/Panel/3.gif" alt="drawing" width="382"/>
<br><br>
<img src="Images/Panel/4.gif" alt="drawing" width="382"/>
<img src="Images/Panel/4.jpg" alt="drawing" width="290"/>
<br><br>

*4. Sanding the panel:*<br>
After the panel was cut with the CNC with a 3mm down-cut mill bit, the panel was sanded with an orbital sander. 
<br><br>
<img src="Images/Panel/5.jpg" alt="drawing" width="350"/>
<br><br>


*5. Finalise panel and add labels:*<br>
A vinyl-cutter was used to cut out labels using graphics from our original 2-D Illustrator file and they were then attached to the board.<br> 
<br><br>
<img src="Images/Panel/6.jpg" alt="drawing" width="290"/>
<img src="Images/Panel/7.gif" alt="drawing" width="382"/>
<br><br>

Finally, an acrylic enamel was used to seal the vinyl letters and give the recycled plastic panel a deeper color and luster.
<br><br>
<img src="Images/Panel/7.JPG" alt="drawing" width="250"/>
<img src="Images/Panel/8.JPG" alt="drawing" width="250"/>
<img src="Images/Panel/8.gif" alt="drawing" width="189"/>
<br><br><br>

#### Step by Step Guide: Fabrication of Buttons
*1. Consider size and dimensions of buttons:*<br>
Based on the 3D-rendered prototype of our artifact, we started considering the form and the size of the buttons. We decided to go with two different types of buttons, one smaller (diameter: 10cm) and one bigger (diameter: 20cm). 
<br><br>
<img src="Images/Buttons/0.jpg" alt="drawing" width="400"/>
<br><br>

*2. Render the two types of buttons in Rhino (Positive):* <br> 
After we drew the sketch on paper, we then continued rendering the buttons in Rhino 7. Here, we could directly include the correct dimensions and sizes and finalise the 3D render.  
<br><br>
<img src="Images/Buttons/1.png" alt="drawing" width="400"/>
<br><br>

*3. Create a mold in 3D for both buttons (Negative):*<br> 
To develop the final mold of our artifact, we had to create the negative of the 3D design in Rhino (Mainly through command 'BooleanDifference'). 
<br><br>
<img src="Images/Buttons/2.png" alt="drawing" width="400"/>
<br>

Throughout the process, we considered rendering both a one-part as well as a two-part mold. Both worked out well, but we decided to go with the one-part mold for the casting. 
<br><br>
<img src="Images/Buttons/3.jpg" alt="drawing" width="400"/>
<img src="Images/Buttons/4.jpg" alt="drawing" width="400"/>
<br><br>

*4. Mix silicone and develop the prototype cast (Positive):* <br> 
After the 3D-printed molds (negative) were ready, we started mixing the silicone and poured it into the mold. We mixed Easyl-940 FDA Parte A & B in the ratio 1:1: After cooling the liquid down, we were able to do an initial press-fit test. 
<br><br>
<img src="Images/Buttons/5.gif" alt="drawing" width="470"/>
<img src="Images/Buttons/6.jpg" alt="drawing" width="200"/>
<br><br>

*5. Repeat process for all remaing buttons:*<br> 
After a successful press-fit test, we were able to repeat this process for all remaining buttons. 
<br><br><br><br>

#### Step by Step Guide: Development of Electronics
*1. Coding and testing:* <br> 
From the framed concept, the logic of the coding was developed. We used Arduino IDE to code and update the code to an Adafruit ESP32 board. To control the LED strips, the FastLED library for Arduino was used. We start testing and iterating (adding more LED strips and buttons until the five where set up) in a breadboard.
<br>

Each LED strip and button, referred to a pin on the board. A 10 kΩ resistance was set for each button.
<br><br>
<img src="Electronics/Code Logic/1.JPG" alt="drawing" width="150"/>
<br><br>

*2. Wires and electronics:* <br> 
The prototype on the breadboard was then transferred to the artefact, adjusting the length of the wires, soldering the resistances and buttons, grouping them, to set a little bit of order, among, LEDs wire, New and Update Button, and the other five to set up the values. Al those were fixed to a wooden board that we have used as a pack support.
<br><br>
<img src="Images/Electronics/1.jpg" alt="drawing" width="300"/>
<img src="Images/Electronics/2.jpg" alt="drawing" width="300"/>
<img src="Images/Electronics/3.jpg" alt="drawing" width="300"/>
<br><br>

*3. Silicon buttons:* <br> 
To ensure the correct functioning of the silicon buttons, we added some extra wooden pieces underneath those, as well as position them according to the upper plastic sheet. Make sure the position matched the holes, and when pressed, the signal was transferred correctly.
<br>
With four screws and nuts, we set the specific separation between the boards, as well as hold everything in position, to be used.
<br><br>
<img src="Images/Electronics/2.gif" alt="drawing" width="350"/>
<img src="Images/Electronics/1.gif" alt="drawing" width="350"/>
<br><br>


#### Materials Used
- Rhino 3D software, and a 3D printer to replicate mold.
- Shredded HDPE and a CNC machine to cut the shield.<br> 
- A 30 x 39 mm board of tbd 7 mm birch plywood .<br> 
- A 30 x 39 mm board of acrylic (self-made shield from shredded and melted HDPE).<br> 
- Ada Fruit ESP 32, 7 Resistors (10k Ohm), 7 Buttons, 35 Cables (Type) + 5 LED strips (12 LEDs).<br> 
- Easyl-940 FDA Parte A & B for the silicone of the mold (1:1).<br> 
<br>

#### Photography
<img src="Images/Final/1.jpg" alt="drawing" width="300"/>
<img src="Images/Final/2.jpg" alt="drawing" width="300"/>
<img src="Images/Final/3.jpg" alt="drawing" width="300"/>
<img src="Images/Final/4.jpg" alt="drawing" width="300"/>
<img src="Images/Final/5.jpg" alt="drawing" width="300"/>
<br><br>

#### Videos
<img src="Images/Final/1.gif" alt="drawing" width="300"/>
<img src="Images/Final/2.gif" alt="drawing" width="300"/>
<br><br><br><br>

### 3. Reflection & Future Work
#### Challenges
- *Molding & Casting:*<br>
When we wanted to print the two-parted mold, the 3D printer was initially not able to carry out the process and stopped during the process. This was mainly due to the small size of the mold as well as the limited space in-between layers. Since we wanted to move forward with the progress, we then decided to re-render the object in an one-part mold which turned out to work well. Since we had some time left in the end, we used another 3D-printer (Prusa) to prototype the two-parts mold. The Prusa printer was ultimately able to deal with the high level of detail. 
<br>

- *Precious Plastics Plate:*<br> 
When we first started melting the HDPE elements for the front plate, the plate could not be used for any further iterations. This was mainly due to the fact that we did not add enough HDPE plastics for the melting process. In another iteration, we added a higher amount of HDPE and were more careful with the temperature (max. 210) and the timinigs (max 12-15 mins total). Still, we slightly burned the surface of the plate - but we worked around this through the process of sanding it at a later stage. 
<br>

- *Installation of Electronics:*<br> 
Due to the lack of time and experience, the position of the electronic elements could still be further improved. For further explorations, we could together with the Fab Lab team explore different opportunitites on how to ideally position the different parts and pieces on the back board. 
<br><br>

#### Future Developments
We are more than happy with this first prototype of this concept the three of us are exploring. We are interested in changing the way we approach the practice (from a design perspective). Developing material to encourage professionals to shift to more sustainable and relational practices. This project aims to become a totemic piece within a studio or company space. Remember professionals of their everyday practice according to a set of topics they are interested to improve, explore or exploit.

We have set five topics to illustrate the concept, but these are not closed. In future developments of the prototype, we would include the artefact in workshops or consultancy developed with its users. After a shared reflection and illustration of topics, they would be able to decide what are the ones that they are more interested in exploring. Even, developing interchangeable labels so those can be modified over time.

If the artefact works as expected, it would become an item where to reflect after each project the professionals endeavour, making them rethink their practice and provoking them to evolve towards better ways of designing.

We would be very interested in having the opportunity to introduce this project to a studio, and develop a workshop around it. Later measure the impact it has have in their practice, reflection on the evolution of their "average".
<br><br>

#### What you find in this repo
- 3D Rhino model files (Panel + Buttons)<br> 
- CNC cut files & Labels (Panel)<br> 
- Coding Logics & Code <br> 
- Images & Videos of the process<br> 
<br><br>

#### Links to personal Posts
Roger Guilemany - [personal webpage](https://roger_guilemany.gitlab.io/mdef-website/term2.html)<br> 
David Wyss - [personal webpage](https://david_wyss.gitlab.io/mdef-class/term3.html)<br>
Krzysztof Wronski - [personal webpage](https://krzysztof_wronski.gitlab.io/wrona/projects.html)
