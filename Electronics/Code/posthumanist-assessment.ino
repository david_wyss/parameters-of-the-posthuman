#include <FastLED.h>

#define NUM_LEDS 12 
#define BTN_0 A0
#define BTN_1 A1
#define BTN_2 21
#define BTN_3 17
#define BTN_4 16

#define BTN_UPDATE 12
#define BTN_NEW 13

#define DATA_PIN0 27
#define DATA_PIN1 33
#define DATA_PIN2 15
#define DATA_PIN3 32
#define DATA_PIN4 14

CRGB leds[5][NUM_LEDS];

int current_values[5];
int average_values[5];
int stored_values[5][10];
int counter = 0;

// false == show average
// true == insert new project
bool state = false;

void setup() {
  pinMode(BTN_0, INPUT_PULLUP);
  pinMode(BTN_1, INPUT_PULLUP);
  pinMode(BTN_2, INPUT_PULLUP);
  pinMode(BTN_3, INPUT_PULLUP);
  pinMode(BTN_4, INPUT_PULLUP);
  pinMode(BTN_UPDATE, INPUT_PULLUP);
  pinMode(BTN_NEW, INPUT_PULLUP);
  delay(500);
  Serial.begin(9600);
  delay(200);
  LEDS.addLeds<WS2813,DATA_PIN0,RGB>(leds[0], NUM_LEDS);
  LEDS.addLeds<WS2813,DATA_PIN1,RGB>(leds[1], NUM_LEDS);
  LEDS.addLeds<WS2813,DATA_PIN2,RGB>(leds[2], NUM_LEDS);
  LEDS.addLeds<WS2813,DATA_PIN3,RGB>(leds[3], NUM_LEDS);
  LEDS.addLeds<WS2813,DATA_PIN4,RGB>(leds[4], NUM_LEDS);
  LEDS.setBrightness(80);
}

void loop() {
  if(state == false) {
    Serial.println("showing average");
    if(digitalRead(BTN_NEW) == LOW) {
      Serial.println("Creating new project");
      current_values[0] = 0;
      current_values[1] = 0;
      current_values[2] = 0;
      current_values[3] = 0;
      current_values[4] = 0;
      turnOffLEDs();
      state = true;
      delay(500);
    }
    displayLEDS();
  } else {
    if(digitalRead(BTN_0) == LOW) {
      current_values[0]++;
      if(current_values[0] > NUM_LEDS) current_values[0] = 0;
      Serial.print("Updated value 0 ");Serial.println(current_values[0]);
      delay(500);
    } else if (digitalRead(BTN_1) == LOW) {
      current_values[1]++;
      if(current_values[1] > NUM_LEDS) current_values[1] = 0;
      Serial.print("Updated value 1 ");Serial.println(current_values[1]);
      delay(500);
    } else if (digitalRead(BTN_2) == LOW) {
      current_values[2]++;
      if(current_values[2] > NUM_LEDS) current_values[2] = 0;
      Serial.print("Updated value 2 ");Serial.println(current_values[2]);
      delay(500);
    } else if (digitalRead(BTN_3) == LOW) {
      current_values[3]++;
      if(current_values[3] > NUM_LEDS) current_values[3] = 0;
      Serial.print("Updated value 3 ");Serial.println(current_values[3]);
      delay(500);
    } else if (digitalRead(BTN_4) == LOW) {
      current_values[4]++;
      if(current_values[4] > NUM_LEDS) current_values[4] = 0;
      Serial.print("Updated value 4 ");Serial.println(current_values[4]);
      delay(500);
    }
   
    if(digitalRead(BTN_UPDATE) == LOW) {
      Serial.println("Storing new project");
      stored_values[0][counter] = current_values[0];
      stored_values[1][counter] = current_values[1];
      stored_values[2][counter] = current_values[2];
      stored_values[3][counter] = current_values[3];
      stored_values[4][counter] = current_values[4];
      state = false;
      counter++;
      average_values[0] = 0;
      average_values[1] = 0;
      average_values[2] = 0;
      average_values[3] = 0;
      average_values[4] = 0;
      for(int i=0;i<counter;i++) {
        average_values[0] += stored_values[0][i];
        average_values[1] += stored_values[1][i];
        average_values[2] += stored_values[2][i];
        average_values[3] += stored_values[3][i];
        average_values[4] += stored_values[4][i];
        Serial.print("Stored at i: "); Serial.print(i);  Serial.print(" -> ");Serial.println(average_values[0]);
      }
       
      Serial.print("Average SUM0:"); Serial.println(average_values[0]);
      Serial.print("Counter:"); Serial.println(counter);
      average_values[0] /= counter;
      average_values[1] /= counter;
      average_values[2] /= counter;
      average_values[3] /= counter;
      average_values[4] /= counter;
      Serial.print("Average val:"); Serial.println(average_values[0]);
      delay(500);
    }
    displayLEDSinput();
  }

}

void displayLEDS() {
  for(int j=0; j<NUM_LEDS; j++) {
    leds[0][j] = (j < average_values[0]) ? CRGB::Green : CRGB::Black;
    leds[1][j] = (j < average_values[1]) ? CRGB::Green : CRGB::Black;
    leds[2][j] = (j < average_values[2]) ? CRGB::Green : CRGB::Black;
    leds[3][j] = (j < average_values[3]) ? CRGB::Green : CRGB::Black;
    leds[4][j] = (j < average_values[4]) ? CRGB::Green : CRGB::Black;
  }
  FastLED.show();
}

void displayLEDSinput() {
  for(int j=0; j<NUM_LEDS; j++) {
    leds[0][j] = (j < current_values[0]) ? CRGB::Green : CRGB::Black;
    leds[1][j] = (j < current_values[1]) ? CRGB::Green : CRGB::Black;
    leds[2][j] = (j < current_values[2]) ? CRGB::Green : CRGB::Black;
    leds[3][j] = (j < current_values[3]) ? CRGB::Green : CRGB::Black;
    leds[4][j] = (j < current_values[4]) ? CRGB::Green : CRGB::Black;
  }
  FastLED.show();
}

void turnOffLEDs() {
  for(int j=0; j<NUM_LEDS; j++) {
    leds[0][j] = CRGB::Black;
    leds[1][j] = CRGB::Black;
    leds[2][j] = CRGB::Black;
    leds[3][j] = CRGB::Black;
    leds[4][j] = CRGB::Black;
  }
  FastLED.show();
}
