(1-SCREWS-3O.nc)
(----------------------------------------------------------------)
( Generated for RhinoCam to export for CNC-STEP models Raptor X-SL3000s20 / T-Rex1215 )
( Postprocessor written at FabLab BCN/IaaC by eduardo.chamorro@iaac.net )
( Last edit : 22 January - 2019 )
(----------------------------------------------------------------)
(Stock Size X = 480.0000, Y =  480.0000, Z = 5.0000)
(Home Origin X =  0, Y = 0, Z = 0)
(Units = MM, Spindle Speed = 18000)
(Max cut depth = Z-5.0000)
(Tool dia= 3.0 mm, Tool length= 35.0 mm)(WARNING!-CHECK YOUR TOOL)
(-------------------------------------------------------------)
%
G90
G64
M7
M8
T1
S18000
M3
G0 Z30.0000
G0 X19.2464 Y12.1044
G1 X19.2464 Y12.1044 Z-1.0000 F3000.
G1 X19.2464 Y12.1044 Z-2.0000 F6000.
G0 Z30.0000
G0 X19.2464 Y12.1044
G0 Z30.0000
G0 X380.7536 Y12.1044
G1 X380.7536 Y12.1044 Z-1.0000 F3000.
G1 X380.7536 Y12.1044 Z-2.0000 F6000.
G0 Z30.0000
G0 X380.7536 Y12.1044
G0 Z30.0000
G0 X376.6219 Y394.9856
G1 X376.6219 Y394.9856 Z-1.0000 F3000.
G1 X376.6219 Y394.9856 Z-2.0000 F6000.
G0 Z30.0000
G0 X376.6219 Y394.9856
G0 Z30.0000
G0 X23.3781 Y394.9856
G1 X23.3781 Y394.9856 Z-1.0000 F3000.
G1 X23.3781 Y394.9856 Z-2.0000 F6000.
G0 Z30.0000
G0 X23.3781 Y394.9856
M9
M5
M30
(--THE END - THANK YOU FOR USING THIS AWESOME POSTPROCESSOR---)
